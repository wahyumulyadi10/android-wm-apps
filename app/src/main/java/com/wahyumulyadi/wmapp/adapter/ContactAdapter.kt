package com.wahyumulyadi.wmapp.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.wahyumulyadi.wmapp.R
import com.wahyumulyadi.wmapp.databinding.ItemCategoryContactBinding
import com.wahyumulyadi.wmapp.databinding.ItemContactBinding
import com.wahyumulyadi.wmapp.model.CategoryModel
import com.wahyumulyadi.wmapp.model.ContactModel

class ContactAdapter:RecyclerView.Adapter<ContactAdapter.ContactViewHolder>() {
    private var dataContact: MutableList<ContactModel> = mutableListOf()
    private var onClickContact: (ContactModel) -> Unit ={}


    inner class ContactViewHolder(val binding: ItemContactBinding):RecyclerView.ViewHolder(
        binding.root
    )
    {
        fun bindView(data: ContactModel,
                     onClickCategory: (ContactModel) -> Unit
        ) {
            binding.tvName.text = data.name
            binding.tvPhone.text = data.phoneNumber
            binding.tvCategory.text =data.category
            binding.ivCall.setOnClickListener {
                onClickCategory(data)
            }

        }
    }

    fun setData(newData: MutableList<ContactModel>){
        dataContact =newData
        notifyDataSetChanged()
    }
    fun addOnclickContactItem(click:(ContactModel)->Unit){
        onClickContact =click
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactAdapter.ContactViewHolder =ContactViewHolder(
        ItemContactBinding.inflate(
            LayoutInflater.from(parent.context),parent,false
        )
    )

    override fun getItemCount(): Int = dataContact.size
    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bindView(dataContact[position],
            onClickContact
        )
    }
}