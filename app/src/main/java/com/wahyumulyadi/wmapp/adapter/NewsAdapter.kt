package com.wahyumulyadi.wmapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wahyumulyadi.wmapp.databinding.ItemNewsBinding
import com.wahyumulyadi.wmapp.model.NewsModel

class NewsAdapter : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    private var dataNews: MutableList<NewsModel> = mutableListOf()
    private var onClickNews: (NewsModel) -> Unit = {}


    fun setData(newData: MutableList<NewsModel>) {
        dataNews = newData
        notifyDataSetChanged()
    }


    fun addOnClickNews(clickNews: (NewsModel) -> Unit) {
        onClickNews = clickNews
    }

    inner class NewsViewHolder(val binding: ItemNewsBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data: NewsModel, onClickNews: (NewsModel) -> Unit) {
            Glide.with(binding.root.context).load(data.image).into(binding.imgNews)
            binding.tvTitle.text = data.title
            binding.tvDescription.text = data.descNews

            binding.itemNews.setOnClickListener {
                onClickNews(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder =
        NewsViewHolder(
            ItemNewsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bindView(dataNews[position], onClickNews)

    }

    override fun getItemCount(): Int = dataNews.size
}
