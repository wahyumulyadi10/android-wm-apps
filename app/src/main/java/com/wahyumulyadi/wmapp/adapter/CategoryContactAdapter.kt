package com.wahyumulyadi.wmapp.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.wahyumulyadi.wmapp.R
import com.wahyumulyadi.wmapp.databinding.ItemCategoryContactBinding
import com.wahyumulyadi.wmapp.model.CategoryModel

class CategoryContactAdapter:RecyclerView.Adapter<CategoryContactAdapter.CategoryContactViewHolder> (){
    private var dataCategory: MutableList<CategoryModel> = mutableListOf()
    private var onClickCategory: (CategoryModel) -> Unit ={}

    inner class CategoryContactViewHolder(val binding: ItemCategoryContactBinding):RecyclerView.ViewHolder(
    binding.root
    ){
        fun bindView(data: CategoryModel,
                     onClickCategory: (CategoryModel) -> Unit
        ) {
            binding.tvCategoryTitle.text = data.title
            binding.constraintCategory.setOnClickListener {
                onClickCategory(data)
            }
            val(selectedBackround, selectColor)= if (data.isSelected?:false){
                Pair(R.drawable.background_rounded_selected, Color.WHITE)
            }
            else{
                Pair(R.drawable.background_rounded_outline, Color.BLACK)
            }
            val selectedBackground = ContextCompat.getDrawable(binding.root.context,selectedBackround)
            binding.constraintCategory.background = selectedBackground
            binding.tvCategoryTitle.setTextColor(selectColor)
        }
    }

    fun setData(newData: MutableList<CategoryModel>){
        dataCategory =newData
        notifyDataSetChanged()
    }
    fun addOnclickCategoryItem(click:(CategoryModel)->Unit){
        onClickCategory =click
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryContactViewHolder =CategoryContactViewHolder(
        ItemCategoryContactBinding.inflate(
            LayoutInflater.from(parent.context),parent,false
        )
    )

    override fun getItemCount(): Int = dataCategory.size
    override fun onBindViewHolder(holder: CategoryContactViewHolder, position: Int) {
        holder.bindView(dataCategory[position],
            onClickCategory
        )
    }
}