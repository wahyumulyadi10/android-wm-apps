package com.wahyumulyadi.wmapp.model

data class CategoryModel (
    val id:Int?,
    val title: String?,
    val isSelected: Boolean?
)