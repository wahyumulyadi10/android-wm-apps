package com.wahyumulyadi.wmapp.model


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsModel(
    val image: String,
    val title: String,
    val descNews: String
) : Parcelable
