package com.wahyumulyadi.wmapp.model

data class ContactModel(
    val image: String,
    val name: String,
    val phoneNumber: String,
    val category: String
)
