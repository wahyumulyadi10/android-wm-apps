package com.wahyumulyadi.wmapp.presentation.fragment

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.wahyumulyadi.wmapp.databinding.FragmentProfileBinding
import com.wahyumulyadi.wmapp.helper.Constant.Companion.PREF_EMAIL
import com.wahyumulyadi.wmapp.helper.Constant.Companion.PREF_IS_LOGIN
import com.wahyumulyadi.wmapp.helper.Constant.Companion.PREF_PHONE
import com.wahyumulyadi.wmapp.helper.Constant.Companion.PREF_USERNAME
import com.wahyumulyadi.wmapp.helper.SharedPrefer
import com.wahyumulyadi.wmapp.presentation.login.LoginActivity

class ProfileFragment :Fragment(){
    private var _binding :FragmentProfileBinding?=null
    private val binding get() = _binding!!
    lateinit var sharedPref: SharedPrefer

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = SharedPrefer(context= requireActivity())

        binding.apply {
            tvEmailUser.text =sharedPref.getString(PREF_EMAIL)
            tvAddressUser.text ="JL. Kenangan"
            tvNameUser.text = sharedPref.getString(PREF_USERNAME)
            tvPhoneUser.text = sharedPref.getString(PREF_PHONE)
            btnLogout.setOnClickListener {
            sharedPref.put(PREF_IS_LOGIN,false)

                val intent = Intent(context, LoginActivity::class.java)
                startActivity(intent)
                close()
            }



        }


    }


    private  fun close(){
        activity?.finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}