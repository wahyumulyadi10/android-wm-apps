package com.wahyumulyadi.wmapp.presentation.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import com.wahyumulyadi.wmapp.adapter.CategoryContactAdapter
import com.wahyumulyadi.wmapp.adapter.ContactAdapter
import com.wahyumulyadi.wmapp.databinding.FragmentContactBinding
import com.wahyumulyadi.wmapp.model.CategoryModel
import com.wahyumulyadi.wmapp.model.ContactModel

class ContactFragment:Fragment() {
    private var _binding : FragmentContactBinding?=null
    private val binding get() = _binding!!
    private val contactAdapter =ContactAdapter()
    private val categoryAdapter = CategoryContactAdapter()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentContactBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        contactAdapter.setData(getContact())
        categoryAdapter.setData(getCategory())

        contactAdapter.addOnclickContactItem {
            callAFriend(it.phoneNumber)
        }
        binding.apply {
            rvCategory.adapter =categoryAdapter
            rvMenu.adapter = contactAdapter
            constraintLayout.searchViewUser.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                   return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    val data = getContact()
                    val filter = data.filter { data ->
                        (data.name?.contains(newText.toString(), ignoreCase = true)
                            ?: false) || (data.category?.contains(newText.toString(), ignoreCase = true)
                            ?: false)
                    }

                    if (newText == null ||newText ==""){
                        contactAdapter.setData(data)
                    }else{
                        contactAdapter.setData(filter.toMutableList())
                    }
                    return true
                }
            })
            

        }
        val dataCategory = getCategory()
        categoryAdapter.setData(dataCategory)
        categoryAdapter.addOnclickCategoryItem {categoryModel->
            val data = getCategory()
            val category = data.map {
                val categoryId = categoryModel.id
                val  isSelected = it.id == categoryId
                it.copy(isSelected = isSelected)

            }
            categoryAdapter.setData(category.toMutableList())
            val contact = getContact()
            val filterData = contact.filter {
                it.category == categoryModel.title

            }
            contactAdapter.setData(filterData.toMutableList())
        }



    }
    private  fun callAFriend(phoneNumber:String){
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNumber")
        startActivity(intent)
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun getCategory(): MutableList<CategoryModel> {

        val listCategoru = mutableListOf(
            CategoryModel(
                id = 1,
                title = "keluarga",
                isSelected = false
            ),
            CategoryModel(
                id = 2,
                title = "kantor",
                isSelected = false
            ),
            CategoryModel(
                id = 3,
                title = "teman",
                isSelected = false
            ),
            CategoryModel(
                id = 4,
                title = "musuh",
                isSelected = false
            )


        )
        return listCategoru
    }

    private fun getContact(): MutableList<ContactModel> {
        val listData = mutableListOf(
            ContactModel(
                name = "Wahyu",
                phoneNumber = "082211552211",
                image = "https://cdnwpseller.gramedia.net/wp-content/uploads/2021/11/26104839/Makanan-Khas-Indonesia-sate-1.jpg",
                category = "keluarga"
            ),
            ContactModel(
                name = "Mul",
                phoneNumber = "082211552211",
                image = "https://cdnwpseller.gramedia.net/wp-content/uploads/2021/11/26104839/Makanan-Khas-Indonesia-sate-1.jpg",
                category = "kantor"
            ),
            ContactModel(
                name = "Yadi",
                phoneNumber = "082211552211",
                image = "https://cdnwpseller.gramedia.net/wp-content/uploads/2021/11/26104839/Makanan-Khas-Indonesia-sate-1.jpg",
                category = "teman"
            ),
            ContactModel(
                name = "Josep",
                phoneNumber = "082211552211",
                image = "https://cdnwpseller.gramedia.net/wp-content/uploads/2021/11/26104839/Makanan-Khas-Indonesia-sate-1.jpg",
                category = "keluarga"
            ),
            ContactModel(
                name = "Bambang",
                phoneNumber = "082211552211",
                image = "https://cdnwpseller.gramedia.net/wp-content/uploads/2021/11/26104839/Makanan-Khas-Indonesia-sate-1.jpg",
                category = "kantor"
            ),
            ContactModel(
                name = "Danu",
                phoneNumber = "082211552211",
                image = "https://cdnwpseller.gramedia.net/wp-content/uploads/2021/11/26104839/Makanan-Khas-Indonesia-sate-1.jpg",
                category = "kantor"
            ),
            ContactModel(
                name = "Karsa",
                phoneNumber = "082211552211",
                image = "https://cdnwpseller.gramedia.net/wp-content/uploads/2021/11/26104839/Makanan-Khas-Indonesia-sate-1.jpg",
                category = "teman"
            ),
            ContactModel(
                name = "Amin",
                phoneNumber = "082211552211",
                image = "https://cdnwpseller.gramedia.net/wp-content/uploads/2021/11/26104839/Makanan-Khas-Indonesia-sate-1.jpg",
                category = "keluarga"
            ),
            ContactModel(
                name = "Dadang",
        phoneNumber = "082211552211",
        image = "https://cdnwpseller.gramedia.net/wp-content/uploads/2021/11/26104839/Makanan-Khas-Indonesia-sate-1.jpg",
        category = "Musuh"
        ),
        )
        return listData

    }
}