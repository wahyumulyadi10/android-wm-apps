package com.wahyumulyadi.wmapp.presentation.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.wahyumulyadi.wmapp.databinding.ActivityRegisterBinding
import com.wahyumulyadi.wmapp.helper.Constant
import com.wahyumulyadi.wmapp.helper.SharedPrefer
import com.wahyumulyadi.wmapp.presentation.login.LoginActivity

class RegisterActivity:AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    lateinit var sharedPref: SharedPrefer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = SharedPrefer(this)
        binding.apply {
            btnDiaSubmit.setOnClickListener {
                val username = etUsername.text
                val email = etEmail.text
                val number = etNumber.text
                val password = etPassword.text

                if (username.isNullOrEmpty() && email.isNullOrEmpty() && password.isNullOrEmpty()){
                    Toast.makeText(applicationContext, "Data Tidak Boleh Kosong", Toast.LENGTH_LONG).show()
                } else {
                    sharedPref.put(Constant.PREF_USERNAME, username.toString())
                    sharedPref.put(Constant.PREF_EMAIL, email.toString())
                    sharedPref.put(Constant.PREF_PASSWORD, password.toString())
                    sharedPref.put(Constant.PREF_PHONE, number.toString())
                    Toast.makeText(applicationContext, "Registrasi Berhasil", Toast.LENGTH_LONG).show()
                    intentTo(LoginActivity::class.java)
                }
            }
        }
    }
    private fun intentTo(classs: Class<*>){
        val intent = Intent(this, classs)
        startActivity(intent)
        finish()
    }
}