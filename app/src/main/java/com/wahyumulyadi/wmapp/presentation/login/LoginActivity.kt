package com.wahyumulyadi.wmapp.presentation.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.wahyumulyadi.wmapp.databinding.ActivityLoginBinding
import com.wahyumulyadi.wmapp.helper.Constant
import com.wahyumulyadi.wmapp.helper.SharedPrefer
import com.wahyumulyadi.wmapp.presentation.MainActivity
import com.wahyumulyadi.wmapp.presentation.register.RegisterActivity

class LoginActivity:AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    lateinit var sharedPref:SharedPrefer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = SharedPrefer(this)

        binding.btnLogin.setOnClickListener {
            val sharedEmail = sharedPref.getString(Constant.PREF_EMAIL)
            val sharedPassword = sharedPref.getString(Constant.PREF_PASSWORD)
            var email = binding.etEmail.text.toString()
            var password = binding.etPassword.text.toString()

            if (email.contains(sharedEmail.toString()) && password.contains(sharedPassword.toString()))
            {
                sharedPref.put(Constant.PREF_IS_LOGIN, true)
                Toast.makeText(applicationContext, "Sukses!", Toast.LENGTH_LONG).show()
                intentTo(MainActivity::class.java)
            }
            else{
                Toast.makeText(applicationContext, "Email atau password salah", Toast.LENGTH_LONG).show()
            }
        }

        binding.btnRegis.setOnClickListener {
            intentTo(RegisterActivity::class.java)
        }
    }
    override fun onStart() {
        super.onStart()
        if (sharedPref.getBoolean(Constant.PREF_IS_LOGIN)){
            intentTo(MainActivity::class.java)
        }
    }

    private fun intentTo(classs: Class<*>){
        val intent = Intent(this, classs)
        startActivity(intent)
        finish()
    }
}