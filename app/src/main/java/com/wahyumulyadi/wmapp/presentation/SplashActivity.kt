package com.wahyumulyadi.wmapp.presentation

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.wahyumulyadi.wmapp.databinding.ActivitySplashBinding
import com.wahyumulyadi.wmapp.helper.Constant.Companion.PREF_IS_LOGIN
import com.wahyumulyadi.wmapp.helper.SharedPrefer
import com.wahyumulyadi.wmapp.presentation.login.LoginActivity
import java.util.*
import kotlin.concurrent.schedule

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    lateinit var sharedPref: SharedPrefer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPref = SharedPrefer(this)
        val isLogin = sharedPref.getBoolean(PREF_IS_LOGIN)
        if (isLogin) {
            Timer().schedule(3000) {
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                finish()
            }
        } else {
            Timer().schedule(3000) {
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }
        }


    }
}