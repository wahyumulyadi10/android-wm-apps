package com.wahyumulyadi.wmapp.presentation

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.wahyumulyadi.wmapp.R
import com.wahyumulyadi.wmapp.databinding.ActivityMainBinding
import com.wahyumulyadi.wmapp.presentation.fragment.ContactFragment
import com.wahyumulyadi.wmapp.presentation.fragment.HomeFragment
import com.wahyumulyadi.wmapp.presentation.fragment.ProfileFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    var backPressedTime: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setContentView(binding.root)

        initBottomNavigation()
        replaceFragment(HomeFragment())
//        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun initBottomNavigation() {
        // set item with menu for bottom navigation and replace fragment base on user click
        binding.bnvMain.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_main_home -> {
                    stopFragment(ContactFragment())
                    stopFragment(ProfileFragment())
                    replaceFragment(HomeFragment())
                    true
                }
                R.id.menu_main_firends -> {
                    stopFragment(HomeFragment())
                    stopFragment(ProfileFragment())
                    replaceFragment(ContactFragment())
                    true
                }
                R.id.menu_main_profile -> {
                    stopFragment(HomeFragment())
                    stopFragment(ContactFragment())
                    replaceFragment(ProfileFragment())
                    true
                }
                else -> false
            }
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_main, fragment)
            .commit()
    }

    // stop fragment
    private fun stopFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .remove(fragment)
            .commit()
    }

    override fun onBackPressed() {

        if (backPressedTime + 3000 > System.currentTimeMillis()) {
            super.onBackPressed()
            finish()
        } else {
            Toast.makeText(this, "Double back  to leave the app.", Toast.LENGTH_LONG).show()
        }
        backPressedTime = System.currentTimeMillis()

    }

}