package com.wahyumulyadi.wmapp.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.wahyumulyadi.wmapp.adapter.NewsAdapter
import com.wahyumulyadi.wmapp.adapter.PopularNewsAdapter
import com.wahyumulyadi.wmapp.databinding.FragmentHomeBinding
import com.wahyumulyadi.wmapp.model.NewsModel

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val newsAdapter = NewsAdapter()
    private val popularNewsAdapter = PopularNewsAdapter()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newsAdapter.setData(populateData())
        popularNewsAdapter.setData(getPopularData())
        binding.rvNewsHorizontal.adapter = popularNewsAdapter
        binding.rvNewsVertical.adapter = newsAdapter


    }

    private fun getPopularData(): MutableList<NewsModel> {
        val listDataPopular = mutableListOf(
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = " Tahu Cry Yuk, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Nasi Goreng ABC",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Ayam Geprek Gembus, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = " Tahu Cry Yuk, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            )
        )
        return listDataPopular
    }

    private fun populateData(): MutableList<NewsModel> {
        val listData = mutableListOf(
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = " Tahu Cry Yuk, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Nasi Goreng ABC",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Ayam Geprek Gembus, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = " Tahu Cry Yuk, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Nasi Goreng ABC",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Ayam Geprek Gembus, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ), NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = " Tahu Cry Yuk, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Nasi Goreng ABC",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Ayam Geprek Gembus, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = " Tahu Cry Yuk, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Nasi Goreng ABC",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            ),
            NewsModel(
                image = "https://static01.nyt.com/images/2020/12/14/well/14google-photo/14google-photo-videoSixteenByNineJumbo1600.jpg",
                title = "Ayam Geprek Gembus, Cipadu",
                descNews = "Sambungin Akun ke Tokopedia, Banyakin Untung"
            )
        )
        return listData
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}